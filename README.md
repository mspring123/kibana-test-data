# kibana test data

generate test data for testing kibana



> Author *`michaelspring123@web.de`*
> > Creation date *`2021-08-10`*
> > > version *`alpha`*


```mermaid
graph LR
    id1(js-generator)-->id2(kibana_jsfake)-->id3(graphic representation)
    style id1 fill:#f9f,stroke:#333,stroke-width:4px
    style id2 fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5
    style id3 fill:#f9,stroke:#333,stroke-width:2px
```
