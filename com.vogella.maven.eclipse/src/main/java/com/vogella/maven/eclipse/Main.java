/**
 * 
 */
package com.vogella.maven.eclipse;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpHost;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.GetAliasesResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.json.JSONArray;
import org.json.JSONObject;

import com.github.javafaker.Faker;

/**
 * @author asdf
 *
 */
public class Main {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		getAllIndex();
//      createIndex();
//      multibulk();
      testbulk("java-multinode", 120000, "localhost", "localhost");
//      singlenode("java-singlenode", 120000, "localhost");
//		strucdoc("localhost", "single-doc");
	}

	/**
	 * @implNote display of all indices that are currently online on the node.
	 * @throws IOException
	 */
	private static void getAllIndex() throws IOException { // on startup
		RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost("localhost", 9200, "http"), new HttpHost("localhost", 9200, "http")));

		try {
			GetAliasesRequest request = new GetAliasesRequest();
			GetAliasesResponse getAliasesResponse = client.indices().getAlias(request, RequestOptions.DEFAULT);
			Map<String, Set<AliasMetaData>> aliases = getAliasesResponse.getAliases();
			Set<String> indices = aliases.keySet();
			for (String key : indices) {
				System.out.println(key);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// on shutdown
		client.close();
	}

	/**
	 * @implNote create a simple index
	 * @throws IOException
	 */
	private static void createIndex() throws IOException {
		// on startup
		RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost("localhost", 9200, "http"), new HttpHost("localhost", 9200, "http")));

		CreateIndexRequest request = new CreateIndexRequest("java-test");
		// Set the number of slices and copies
//      request.settings(Settings.builder().put("index.number_of_shards", 3).put("index.number_of_replicas", 2));
		// Field mapping
		Map<String, Object> message = new HashMap<String, Object>();
		message.put("type", "text");
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("message", message);
		Map<String, Object> mapping = new HashMap<String, Object>();
		mapping.put("properties", properties);
		request.mapping(mapping);

		// Execute create request
		CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
		boolean acknowledged = createIndexResponse.isAcknowledged();
		boolean shardsAcknowledged = createIndexResponse.isShardsAcknowledged();
		System.out.println(acknowledged && shardsAcknowledged);

		// on shutdown
		client.close();
	}

	/**
	 * @implNot multiple bulk operations similar to sql batch processing
	 * @throws IOException
	 */
	private static void multibulk() throws IOException {
		String defaultIndex = "java-test";

		// on startup
		RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost("localhost", 9200, "http"), new HttpHost("localhost", 9200, "http")));

		// Multiple requests.It can be the same add request or different types of
		// requests
		BulkRequest request = new BulkRequest();

		// Add request
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		jsonMap.put("type", "java");
		jsonMap.put("content", "java NP");
		jsonMap.put("creator", "creator2");
		jsonMap.put("createTime", new Date());
		request.add(new IndexRequest(defaultIndex).id("2").source(jsonMap));
		// Modification request
		Map<String, Object> jsonMap2 = new HashMap<String, Object>();
		jsonMap2.put("content", "java NP!!!!");
		request.add(new UpdateRequest(defaultIndex, "1").doc(jsonMap2));

		BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
		for (BulkItemResponse bulkItemResponse : bulkResponse) {
			DocWriteResponse itemResponse = bulkItemResponse.getResponse();

			switch (bulkItemResponse.getOpType()) {
			case INDEX:
			case CREATE:
				IndexResponse indexResponse = (IndexResponse) itemResponse;
				System.out.println("create " + indexResponse);
				break;
			case UPDATE:
				UpdateResponse updateResponse = (UpdateResponse) itemResponse;
				System.out.println("updateResponse " + updateResponse);
				break;
			case DELETE:
				DeleteResponse deleteResponse = (DeleteResponse) itemResponse;
			}
		}

		// on shutdown
		client.close();
	}

	/**
	 * @implNote method that creates a nested document in batch.
	 * @param indexname
	 * @param numberOfdoc
	 * @param node_a
	 * @param node_b
	 * @throws IOException
	 */
	public static void testbulk(String indexname, int numberOfdoc, String node_a, String node_b) throws IOException {
		Faker faker = new Faker(new Locale("en-GB"));
//  	Map<String, JsonValue> object;
//    Object object = new JSONObject()
//            .put("cityName", faker.address().cityName())
//            .put("country", faker.address().country())
//            .put("book", new JSONObject().put("book", faker.book().title())).toMap();

		System.out.println("Start date " + new Date());

		// on startup
		RestHighLevelClient client = new RestHighLevelClient(RestClient
				.builder(new HttpHost(node_a.toString(), 9200, "http"), new HttpHost(node_b.toString(), 9200, "http")));

		// Multiple requests.It can be the same add request or different types of
		// requests
		BulkRequest request = new BulkRequest();

		long duration_total = 0l;
		// Add request
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		for (int a = 1; a <= numberOfdoc; a++) {
			jsonMap.put("type", faker.name().hashCode());
			jsonMap.put("content", faker.ancient().titan());
			jsonMap.put("creator", faker.name().lastName());
			jsonMap.put("createTime", new Date());
			Object object = new JSONObject().put("cityName", faker.address().cityName())
					.put("country", faker.address().country())
					.put("books", new JSONObject().put("book", faker.book().title())).toMap();
			jsonMap.put("nested", object);
			request.add(new IndexRequest(indexname).id(String.valueOf(a)).source(jsonMap));
			if (a % 10000 == 0) {
				long startTime = System.nanoTime();
				System.out.println("execute the statement at " + String.valueOf(a) + " docs");
				BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
				long endTime = System.nanoTime();
				long duration = (endTime - startTime) / 1000000; // divide by 1000000 to get milliseconds.
				System.out.println("That took " + (duration) + " milliseconds");
				System.out.println("**********************************");
				duration_total += duration;
			}
		}
		long seconds = (duration_total / 1000);
		System.out.println("That took duration_total " + (duration_total) + " milliseconds");
		System.out.println("duration_total " + (seconds) + " seconds");
		System.out.println("End date " + new Date());
	}

	/**
	 * @implNote copy and paste method for the single node
	 * @param indexname
	 * @param numberOfdoc
	 * @param node
	 * @throws IOException
	 */
	public static void singlenode(String indexname, int numberOfdoc, String node) throws IOException {
		Faker faker = new Faker(new Locale("en-GB"));
//  	Map<String, JsonValue> object;
//    Object object = new JSONObject()
//            .put("cityName", faker.address().cityName())
//            .put("country", faker.address().country())
//            .put("book", new JSONObject().put("book", faker.book().title())).toMap();

		System.out.println("Start date " + new Date());

		// on startup
		RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(node.toString(), 9200, "http")));

		// Multiple requests.It can be the same add request or different types of
		// requests
		BulkRequest request = new BulkRequest();

		long duration_total = 0l;
		// Add request
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		for (int a = 1; a <= numberOfdoc; a++) {
			jsonMap.put("type", faker.name().hashCode());
			jsonMap.put("content", faker.ancient().titan());
			jsonMap.put("creator", faker.name().lastName());
			jsonMap.put("createTime", new Date());
			jsonMap.put("asdf", null);
			jsonMap.put("quote", faker.backToTheFuture().quote());
			Object object = new JSONObject().put("cityName", faker.address().cityName())
					.put("country", faker.address().country())
					.put("books", new JSONObject().put("book", faker.book().title())).toMap();
			jsonMap.put("nested", object);
			request.add(new IndexRequest(indexname).id(String.valueOf(a)).source(jsonMap));
			if (a % 10000 == 0) {
				long startTime = System.nanoTime();
				System.out.println("execute the statement at " + String.valueOf(a) + " docs");
				BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
				long endTime = System.nanoTime();
				long duration = (endTime - startTime) / 1000000; // divide by 1000000 to get milliseconds.
				System.out.println("That took " + (duration) + " milliseconds");
				System.out.println("**********************************");
				duration_total += duration;
			}
		}
		long seconds = (duration_total / 1000);
		System.out.println("That took duration_total " + (duration_total) + " milliseconds");
		System.out.println("duration_total " + (seconds) + " seconds");
		System.out.println("End date " + new Date());
	}

	/**
	 * @implNote insert a nested single doc
	 * @param node
	 * @param index_name
	 * @throws IOException
	 */
	public static void strucdoc(String node, String index_name) throws IOException {

		System.out.println("Start date " + new Date());

		// on startup
		RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(node.toString(), 9200, "http")));
		// Multiple requests.It can be the same add request or different types of
		// requests
		BulkRequest request = new BulkRequest();

		long doc_id = 126l;
		long duration_total = 0l;
		Map<String, Object> singledoc = new HashMap<String, Object>();
		singledoc.put("docid", doc_id);
		singledoc.put("doctitle", "#8A2BE2");
		singledoc.put("timestamp", "2012-06-12 13:43:37");

//  		first array doc
		Object parameter_alpha = new JSONObject().put("parameter", 126).put("elastic value", 5.746)
				.put("meaning", "crocyldefix").put("asdf-test", new JSONObject().put("asdf-boolean", false)).toMap();

//  		second array doc
		Object parameter_beta = new JSONObject().put("parameter", 126).put("elastic value", 6.601)
				.put("meaning", "roll the dice without a chessboard")
				.put("asdf-test", new JSONObject().put("asdf-boolean", true).put("float_value", 46.8f)).toMap();

//  		Object json_array = new JSONArray().put(parameter_alpha);
		Object json_array = new JSONArray().put(parameter_alpha).put(parameter_beta).toList();
		singledoc.put("json_array", json_array);

		request.add(new IndexRequest(index_name).id(String.valueOf(doc_id)).source(singledoc));
		long startTime = System.nanoTime();
		System.out.println("execute the statement at " + String.valueOf(doc_id) + " docs");
		BulkResponse bulkResponse = client.bulk(request, RequestOptions.DEFAULT);
		long endTime = System.nanoTime();
		long duration = (endTime - startTime) / 1000000; // divide by 1000000 to get milliseconds.
		System.out.println("That took " + (duration) + " milliseconds");
		System.out.println("**********************************");
		duration_total += duration;

		long seconds = (duration_total / 1000);
		System.out.println("That took duration_total " + (duration_total) + " milliseconds");
		System.out.println("duration_total " + (seconds) + " seconds");
		System.out.println("End date " + new Date());

	}

}
