/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

const { Client } = require('@elastic/elasticsearch');

const client = new Client({
    node: 'http://localhost:9200'
});


/* Delete index */
client.indices.delete({
    index: 'kibana_jsfake',
}).then(function (resp) {
    console.log("Successful query!");
    console.log(JSON.stringify(resp, null, 4));
}, function (err) {
    console.trace(err.message);
});