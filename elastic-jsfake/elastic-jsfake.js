/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

require('array.prototype.flatmap').shim();
var faker = require('faker');
const cliProgress = require('cli-progress');
const { Client } = require('@elastic/elasticsearch');
const client = new Client({
    node: 'http://localhost:9200'
});


async function generate () {
    await client.indices.create({
        index: 'kibana_jsfake',
        body: {
            mappings: {
                properties: {
                    id: { type: 'text' },
                    array: { type: 'keyword' },
                    insert_date: { type: 'date' },
                    location: {
                        type: "geo_point"
                    },
                    internet: {
                        properties: {
                            ip_addr: { type: 'ip' },
                            past: { type: 'date' }
                        }
                    },
                    person: {
                        properties: {
                            firstName: { type: 'text' },
                            lastName: { type: 'text' },
                            jobTitle: { type: 'keyword' },
                            gender: { type: 'keyword' }
                        }
                    },
                    commerce: {
                        properties: {
                            productName: { type: 'keyword' },
                            productAdjective: { type: 'keyword' },
                            price: { type: 'float' },
                            product: {
                                "type": "nested",
                                properties: {
                                    productMaterial: { type: 'keyword' },
                                    productDescription: { type: 'text' }
                                }

                            }
                        }
                    }
                }
            } }
    }, { ignore: [ 400 ] });

    const dataset = [ ];

    try {
//        const dataset = [ ];
        var i = 0;
        do {
            i += 1;
            var document = {
                id: faker.datatype.uuid(),
                array: faker.datatype.array().toString(),
                insert_date: new Date(),
                location: {
                    lat: faker.address.latitude(),
                    lon: faker.address.longitude()
                },
                internet: {
                    ip_addr: faker.internet.ip(),
                    past: faker.date.past()
                },
                person: {
                    firstName: faker.name.firstName(),
                    lastName: faker.name.lastName(),
                    jobTitle: faker.name.jobTitle(),
                    gender: faker.name.gender()
                },
                commerce: {
                    productName: faker.commerce.productName(),
                    productAdjective: faker.commerce.productAdjective(),
                    price: parseFloat(faker.commerce.price()),
                    product: {
                        productMaterial: faker.commerce.productMaterial(),
                        productDescription: faker.commerce.productDescription()
                    }
                }
//                creates a nested object without previously defined mapping
//                asdf: faker.helpers.createCard()
            };

//            add documents to the array
            dataset.push(document);

//            performs the insert every 10k documents which is the maximum of elastic
            if ( i % 2642 === 0 ) {
                console.log('\n send off at ' + i);

                const body = dataset.flatMap(doc => [ { index: { _index: 'kibana_jsfake' } }, doc ]);

                const { body: bulkResponse } = await client.bulk({ refresh: true, body });

//                emptying the set using the pop method is the best way to perform
                while ( dataset.length > 0 ) {
                    dataset.pop();
                }

                if ( bulkResponse.errors ) {
                    const erroredDocuments = [ ];
                    // The items array has the same order of the dataset we just indexed.
                    // The presence of the `error` key indicates that the operation
                    // that we did for the document has failed.
                    bulkResponse.items.forEach((action, i) => {
                        const operation = Object.keys(action)[0];
                        if ( action[operation].error ) {
                            erroredDocuments.push({
                                // If the status is 429 it means that you can retry the document,
                                // otherwise it's very likely a mapping error, and you should
                                // fix the document before to try it again.
                                status: action[operation].status,
                                error: action[operation].error,
                                operation: body[i * 2],
                                document: body[i * 2 + 1]
                            });
                        }
                    });
                    console.log(erroredDocuments);
                }
            }

            // create a new progress bar instance and use shades_classic theme
            const bar1 = new cliProgress.SingleBar({ }, cliProgress.Presets.shades_classic);

            // start the progress bar with a total value of 200 and start value of 0
            bar1.start(2642, i);

            // stop the progress bar
//            bar1.stop();


        } while ( i < 2642 );
    } catch ( err ) { // <-- the "error object", could use another word instead of err
        console.error('error kibana_jsfake');
        // ...
    }


//    displays the number of indexed documents
    const { body: count } = await client.count({ index: 'kibana_jsfake' });
    console.log(count);
}

generate().catch(console.log);